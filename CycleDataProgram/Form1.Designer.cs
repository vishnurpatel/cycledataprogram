﻿namespace CycleDataProgram
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.Modlelable = new System.Windows.Forms.Label();
            this.Datelabel = new System.Windows.Forms.Label();
            this.VersionLable = new System.Windows.Forms.Label();
            this.StartTimelabel = new System.Windows.Forms.Label();
            this.Lengthlabel = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.KMH = new System.Windows.Forms.RadioButton();
            this.MPH = new System.Windows.Forms.RadioButton();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PWShowBut = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RPMShowBut = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.HRShowBut = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SPShowBut = new System.Windows.Forms.RadioButton();
            this.SPHideBut = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.ALTShowBut = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Intervallabel = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.MinALTlabel = new System.Windows.Forms.Label();
            this.AvgALTlabel = new System.Windows.Forms.Label();
            this.MaxALTlabel = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.MinHRlabel = new System.Windows.Forms.Label();
            this.AvgHRlabel = new System.Windows.Forms.Label();
            this.MaxHRlabel = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.Dicslabel = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.AvgPWlabel = new System.Windows.Forms.Label();
            this.MaxPWlabel = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.AvgSPlabel = new System.Windows.Forms.Label();
            this.MaxSPlabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.SpeedButton = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.selectedRows = new System.Windows.Forms.Button();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelnp = new System.Windows.Forms.Label();
            this.labelif = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labeltss = new System.Windows.Forms.Label();
            this.labelUMH = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(1078, 183);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(379, 176);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // Modlelable
            // 
            this.Modlelable.AutoSize = true;
            this.Modlelable.Location = new System.Drawing.Point(6, 28);
            this.Modlelable.Name = "Modlelable";
            this.Modlelable.Size = new System.Drawing.Size(35, 13);
            this.Modlelable.TabIndex = 2;
            this.Modlelable.Text = "label1";
            // 
            // Datelabel
            // 
            this.Datelabel.AutoSize = true;
            this.Datelabel.Location = new System.Drawing.Point(154, 15);
            this.Datelabel.Name = "Datelabel";
            this.Datelabel.Size = new System.Drawing.Size(35, 13);
            this.Datelabel.TabIndex = 3;
            this.Datelabel.Text = "label1";
            // 
            // VersionLable
            // 
            this.VersionLable.AutoSize = true;
            this.VersionLable.Location = new System.Drawing.Point(6, 15);
            this.VersionLable.Name = "VersionLable";
            this.VersionLable.Size = new System.Drawing.Size(35, 13);
            this.VersionLable.TabIndex = 4;
            this.VersionLable.Text = "label1";
            // 
            // StartTimelabel
            // 
            this.StartTimelabel.AutoSize = true;
            this.StartTimelabel.Location = new System.Drawing.Point(154, 28);
            this.StartTimelabel.Name = "StartTimelabel";
            this.StartTimelabel.Size = new System.Drawing.Size(35, 13);
            this.StartTimelabel.TabIndex = 5;
            this.StartTimelabel.Text = "label1";
            // 
            // Lengthlabel
            // 
            this.Lengthlabel.AutoSize = true;
            this.Lengthlabel.Location = new System.Drawing.Point(154, 41);
            this.Lengthlabel.Name = "Lengthlabel";
            this.Lengthlabel.Size = new System.Drawing.Size(35, 13);
            this.Lengthlabel.TabIndex = 6;
            this.Lengthlabel.Text = "label1";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(764, 330);
            this.dataGridView1.TabIndex = 7;
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.IsShowPointValues = false;
            this.zedGraphControl1.Location = new System.Drawing.Point(12, 363);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.PointValueFormat = "G";
            this.zedGraphControl1.Size = new System.Drawing.Size(1445, 591);
            this.zedGraphControl1.TabIndex = 12;
            // 
            // KMH
            // 
            this.KMH.AutoSize = true;
            this.KMH.Location = new System.Drawing.Point(6, 42);
            this.KMH.Name = "KMH";
            this.KMH.Size = new System.Drawing.Size(54, 17);
            this.KMH.TabIndex = 18;
            this.KMH.Text = "KM/H";
            this.KMH.UseVisualStyleBackColor = true;
            this.KMH.MouseClick += new System.Windows.Forms.MouseEventHandler(this.KMH_MouseClick);
            // 
            // MPH
            // 
            this.MPH.AutoSize = true;
            this.MPH.Location = new System.Drawing.Point(6, 19);
            this.MPH.Name = "MPH";
            this.MPH.Size = new System.Drawing.Size(49, 17);
            this.MPH.TabIndex = 17;
            this.MPH.Text = "MPH";
            this.MPH.UseVisualStyleBackColor = true;
            this.MPH.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MPH_MouseClick);
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.MPH);
            this.groupBox.Controls.Add(this.KMH);
            this.groupBox.Location = new System.Drawing.Point(787, 221);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(75, 65);
            this.groupBox.TabIndex = 23;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "Euro/US";
            this.groupBox.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PWShowBut);
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Location = new System.Drawing.Point(993, 292);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(75, 65);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Power";
            this.groupBox2.Visible = false;
            // 
            // PWShowBut
            // 
            this.PWShowBut.AutoSize = true;
            this.PWShowBut.Location = new System.Drawing.Point(6, 19);
            this.PWShowBut.Name = "PWShowBut";
            this.PWShowBut.Size = new System.Drawing.Size(52, 17);
            this.PWShowBut.TabIndex = 17;
            this.PWShowBut.Text = "Show";
            this.PWShowBut.UseVisualStyleBackColor = true;
            this.PWShowBut.CheckedChanged += new System.EventHandler(this.HRShowBut_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Checked = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 42);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(47, 17);
            this.radioButton2.TabIndex = 18;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Hide";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.HRShowBut_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.RPMShowBut);
            this.groupBox3.Controls.Add(this.radioButton4);
            this.groupBox3.Location = new System.Drawing.Point(889, 292);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(75, 65);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "RPM";
            this.groupBox3.Visible = false;
            // 
            // RPMShowBut
            // 
            this.RPMShowBut.AutoSize = true;
            this.RPMShowBut.Location = new System.Drawing.Point(6, 19);
            this.RPMShowBut.Name = "RPMShowBut";
            this.RPMShowBut.Size = new System.Drawing.Size(52, 17);
            this.RPMShowBut.TabIndex = 17;
            this.RPMShowBut.Text = "Show";
            this.RPMShowBut.UseVisualStyleBackColor = true;
            this.RPMShowBut.CheckedChanged += new System.EventHandler(this.HRShowBut_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(6, 42);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(47, 17);
            this.radioButton4.TabIndex = 18;
            this.radioButton4.Text = "Hide";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.HRShowBut_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.HRShowBut);
            this.groupBox4.Controls.Add(this.radioButton8);
            this.groupBox4.Location = new System.Drawing.Point(889, 221);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(75, 65);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "HartRate";
            this.groupBox4.Visible = false;
            // 
            // HRShowBut
            // 
            this.HRShowBut.AutoSize = true;
            this.HRShowBut.Location = new System.Drawing.Point(6, 19);
            this.HRShowBut.Name = "HRShowBut";
            this.HRShowBut.Size = new System.Drawing.Size(52, 17);
            this.HRShowBut.TabIndex = 17;
            this.HRShowBut.Text = "Show";
            this.HRShowBut.UseVisualStyleBackColor = true;
            this.HRShowBut.CheckedChanged += new System.EventHandler(this.HRShowBut_CheckedChanged);
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(6, 42);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(47, 17);
            this.radioButton8.TabIndex = 18;
            this.radioButton8.Text = "Hide";
            this.radioButton8.UseVisualStyleBackColor = true;
            this.radioButton8.CheckedChanged += new System.EventHandler(this.HRShowBut_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SPShowBut);
            this.groupBox1.Controls.Add(this.SPHideBut);
            this.groupBox1.Location = new System.Drawing.Point(787, 292);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(75, 65);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Speed";
            this.groupBox1.Visible = false;
            // 
            // SPShowBut
            // 
            this.SPShowBut.AutoSize = true;
            this.SPShowBut.Location = new System.Drawing.Point(6, 19);
            this.SPShowBut.Name = "SPShowBut";
            this.SPShowBut.Size = new System.Drawing.Size(52, 17);
            this.SPShowBut.TabIndex = 17;
            this.SPShowBut.Text = "Show";
            this.SPShowBut.UseVisualStyleBackColor = true;
            this.SPShowBut.CheckedChanged += new System.EventHandler(this.HRShowBut_CheckedChanged);
            // 
            // SPHideBut
            // 
            this.SPHideBut.AutoSize = true;
            this.SPHideBut.Location = new System.Drawing.Point(6, 42);
            this.SPHideBut.Name = "SPHideBut";
            this.SPHideBut.Size = new System.Drawing.Size(47, 17);
            this.SPHideBut.TabIndex = 18;
            this.SPHideBut.Text = "Hide";
            this.SPHideBut.UseVisualStyleBackColor = true;
            this.SPHideBut.CheckedChanged += new System.EventHandler(this.HRShowBut_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.ALTShowBut);
            this.groupBox6.Controls.Add(this.radioButton12);
            this.groupBox6.Location = new System.Drawing.Point(993, 221);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(75, 65);
            this.groupBox6.TabIndex = 24;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Altitude";
            this.groupBox6.Visible = false;
            // 
            // ALTShowBut
            // 
            this.ALTShowBut.AutoSize = true;
            this.ALTShowBut.Location = new System.Drawing.Point(6, 19);
            this.ALTShowBut.Name = "ALTShowBut";
            this.ALTShowBut.Size = new System.Drawing.Size(52, 17);
            this.ALTShowBut.TabIndex = 17;
            this.ALTShowBut.Text = "Show";
            this.ALTShowBut.UseVisualStyleBackColor = true;
            this.ALTShowBut.CheckedChanged += new System.EventHandler(this.HRShowBut_CheckedChanged);
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Checked = true;
            this.radioButton12.Location = new System.Drawing.Point(6, 42);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(47, 17);
            this.radioButton12.TabIndex = 18;
            this.radioButton12.TabStop = true;
            this.radioButton12.Text = "Hide";
            this.radioButton12.UseVisualStyleBackColor = true;
            this.radioButton12.CheckedChanged += new System.EventHandler(this.HRShowBut_CheckedChanged);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Highlight;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1469, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // Intervallabel
            // 
            this.Intervallabel.AutoSize = true;
            this.Intervallabel.Location = new System.Drawing.Point(6, 41);
            this.Intervallabel.Name = "Intervallabel";
            this.Intervallabel.Size = new System.Drawing.Size(35, 13);
            this.Intervallabel.TabIndex = 7;
            this.Intervallabel.Text = "label1";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.MinALTlabel);
            this.groupBox8.Controls.Add(this.AvgALTlabel);
            this.groupBox8.Controls.Add(this.MaxALTlabel);
            this.groupBox8.Location = new System.Drawing.Point(930, 154);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(138, 61);
            this.groupBox8.TabIndex = 32;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Alittude";
            this.groupBox8.Visible = false;
            // 
            // MinALTlabel
            // 
            this.MinALTlabel.AutoSize = true;
            this.MinALTlabel.Location = new System.Drawing.Point(6, 42);
            this.MinALTlabel.Name = "MinALTlabel";
            this.MinALTlabel.Size = new System.Drawing.Size(35, 13);
            this.MinALTlabel.TabIndex = 29;
            this.MinALTlabel.Text = "label1";
            // 
            // AvgALTlabel
            // 
            this.AvgALTlabel.AutoSize = true;
            this.AvgALTlabel.Location = new System.Drawing.Point(6, 16);
            this.AvgALTlabel.Name = "AvgALTlabel";
            this.AvgALTlabel.Size = new System.Drawing.Size(35, 13);
            this.AvgALTlabel.TabIndex = 28;
            this.AvgALTlabel.Text = "label1";
            // 
            // MaxALTlabel
            // 
            this.MaxALTlabel.AutoSize = true;
            this.MaxALTlabel.Location = new System.Drawing.Point(6, 29);
            this.MaxALTlabel.Name = "MaxALTlabel";
            this.MaxALTlabel.Size = new System.Drawing.Size(35, 13);
            this.MaxALTlabel.TabIndex = 27;
            this.MaxALTlabel.Text = "label1";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.MinHRlabel);
            this.groupBox9.Controls.Add(this.AvgHRlabel);
            this.groupBox9.Controls.Add(this.MaxHRlabel);
            this.groupBox9.Location = new System.Drawing.Point(782, 154);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(138, 61);
            this.groupBox9.TabIndex = 33;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Hart Rate";
            this.groupBox9.Visible = false;
            // 
            // MinHRlabel
            // 
            this.MinHRlabel.AutoSize = true;
            this.MinHRlabel.Location = new System.Drawing.Point(6, 42);
            this.MinHRlabel.Name = "MinHRlabel";
            this.MinHRlabel.Size = new System.Drawing.Size(35, 13);
            this.MinHRlabel.TabIndex = 29;
            this.MinHRlabel.Text = "label2";
            // 
            // AvgHRlabel
            // 
            this.AvgHRlabel.AutoSize = true;
            this.AvgHRlabel.Location = new System.Drawing.Point(6, 16);
            this.AvgHRlabel.Name = "AvgHRlabel";
            this.AvgHRlabel.Size = new System.Drawing.Size(35, 13);
            this.AvgHRlabel.TabIndex = 28;
            this.AvgHRlabel.Text = "label1";
            // 
            // MaxHRlabel
            // 
            this.MaxHRlabel.AutoSize = true;
            this.MaxHRlabel.Location = new System.Drawing.Point(6, 29);
            this.MaxHRlabel.Name = "MaxHRlabel";
            this.MaxHRlabel.Size = new System.Drawing.Size(35, 13);
            this.MaxHRlabel.TabIndex = 27;
            this.MaxHRlabel.Text = "label1";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.Dicslabel);
            this.groupBox10.Controls.Add(this.Modlelable);
            this.groupBox10.Controls.Add(this.Datelabel);
            this.groupBox10.Controls.Add(this.VersionLable);
            this.groupBox10.Controls.Add(this.StartTimelabel);
            this.groupBox10.Controls.Add(this.Lengthlabel);
            this.groupBox10.Controls.Add(this.Intervallabel);
            this.groupBox10.Location = new System.Drawing.Point(782, 27);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(286, 72);
            this.groupBox10.TabIndex = 34;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Head Data Info";
            this.groupBox10.Visible = false;
            // 
            // Dicslabel
            // 
            this.Dicslabel.AutoSize = true;
            this.Dicslabel.Location = new System.Drawing.Point(6, 54);
            this.Dicslabel.Name = "Dicslabel";
            this.Dicslabel.Size = new System.Drawing.Size(35, 13);
            this.Dicslabel.TabIndex = 37;
            this.Dicslabel.Text = "label1";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.AvgPWlabel);
            this.groupBox7.Controls.Add(this.MaxPWlabel);
            this.groupBox7.Location = new System.Drawing.Point(930, 106);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(138, 48);
            this.groupBox7.TabIndex = 35;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Power";
            this.groupBox7.Visible = false;
            // 
            // AvgPWlabel
            // 
            this.AvgPWlabel.AutoSize = true;
            this.AvgPWlabel.Location = new System.Drawing.Point(6, 16);
            this.AvgPWlabel.Name = "AvgPWlabel";
            this.AvgPWlabel.Size = new System.Drawing.Size(35, 13);
            this.AvgPWlabel.TabIndex = 28;
            this.AvgPWlabel.Text = "label1";
            // 
            // MaxPWlabel
            // 
            this.MaxPWlabel.AutoSize = true;
            this.MaxPWlabel.Location = new System.Drawing.Point(6, 29);
            this.MaxPWlabel.Name = "MaxPWlabel";
            this.MaxPWlabel.Size = new System.Drawing.Size(35, 13);
            this.MaxPWlabel.TabIndex = 27;
            this.MaxPWlabel.Text = "label1";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.AvgSPlabel);
            this.groupBox5.Controls.Add(this.MaxSPlabel);
            this.groupBox5.Location = new System.Drawing.Point(782, 106);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(138, 48);
            this.groupBox5.TabIndex = 36;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Speed";
            this.groupBox5.Visible = false;
            // 
            // AvgSPlabel
            // 
            this.AvgSPlabel.AutoSize = true;
            this.AvgSPlabel.Location = new System.Drawing.Point(6, 16);
            this.AvgSPlabel.Name = "AvgSPlabel";
            this.AvgSPlabel.Size = new System.Drawing.Size(35, 13);
            this.AvgSPlabel.TabIndex = 28;
            this.AvgSPlabel.Text = "label1";
            // 
            // MaxSPlabel
            // 
            this.MaxSPlabel.AutoSize = true;
            this.MaxSPlabel.Location = new System.Drawing.Point(6, 29);
            this.MaxSPlabel.Name = "MaxSPlabel";
            this.MaxSPlabel.Size = new System.Drawing.Size(35, 13);
            this.MaxSPlabel.TabIndex = 27;
            this.MaxSPlabel.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(821, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 24);
            this.label1.TabIndex = 37;
            this.label1.Text = "Cycle Data File Reader";
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox11.Controls.Add(this.SpeedButton);
            this.groupBox11.Controls.Add(this.radioButton1);
            this.groupBox11.Location = new System.Drawing.Point(697, 919);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox11.Size = new System.Drawing.Size(143, 35);
            this.groupBox11.TabIndex = 38;
            this.groupBox11.TabStop = false;
            this.groupBox11.Visible = false;
            // 
            // SpeedButton
            // 
            this.SpeedButton.AutoSize = true;
            this.SpeedButton.Location = new System.Drawing.Point(79, 11);
            this.SpeedButton.Name = "SpeedButton";
            this.SpeedButton.Size = new System.Drawing.Size(56, 17);
            this.SpeedButton.TabIndex = 1;
            this.SpeedButton.Text = "Speed";
            this.SpeedButton.UseVisualStyleBackColor = true;
            this.SpeedButton.CheckedChanged += new System.EventHandler(this.HRShowBut_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(4, 11);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(68, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Dictence";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // selectedRows
            // 
            this.selectedRows.Location = new System.Drawing.Point(1074, 120);
            this.selectedRows.Name = "selectedRows";
            this.selectedRows.Size = new System.Drawing.Size(141, 23);
            this.selectedRows.TabIndex = 41;
            this.selectedRows.Text = "Calculate Selected Data";
            this.selectedRows.UseVisualStyleBackColor = true;
            this.selectedRows.Visible = false;
            this.selectedRows.Click += new System.EventHandler(this.selectedRows_Click);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(99, 21);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 44;
            this.monthCalendar1.Visible = false;
            this.monthCalendar1.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateSelected);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(1224, 122);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(59, 20);
            this.textBox1.TabIndex = 45;
            this.textBox1.Text = "Enter FTP";
            this.textBox1.Visible = false;
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1289, 120);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 46;
            this.button1.Text = "TSS";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1221, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 47;
            this.label2.Text = "Normalized Power";
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1221, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 48;
            this.label3.Text = "Intensity Factor";
            this.label3.Visible = false;
            // 
            // labelnp
            // 
            this.labelnp.AutoSize = true;
            this.labelnp.Location = new System.Drawing.Point(1323, 145);
            this.labelnp.Name = "labelnp";
            this.labelnp.Size = new System.Drawing.Size(35, 13);
            this.labelnp.TabIndex = 49;
            this.labelnp.Text = "label4";
            this.labelnp.Visible = false;
            // 
            // labelif
            // 
            this.labelif.AutoSize = true;
            this.labelif.Location = new System.Drawing.Point(1323, 162);
            this.labelif.Name = "labelif";
            this.labelif.Size = new System.Drawing.Size(35, 13);
            this.labelif.TabIndex = 50;
            this.labelif.Text = "label5";
            this.labelif.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1331, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 51;
            // 
            // labeltss
            // 
            this.labeltss.AutoSize = true;
            this.labeltss.Location = new System.Drawing.Point(1370, 125);
            this.labeltss.Name = "labeltss";
            this.labeltss.Size = new System.Drawing.Size(35, 13);
            this.labeltss.TabIndex = 52;
            this.labeltss.Text = "label5";
            this.labeltss.Visible = false;
            // 
            // labelUMH
            // 
            this.labelUMH.AutoSize = true;
            this.labelUMH.Location = new System.Drawing.Point(1075, 27);
            this.labelUMH.Name = "labelUMH";
            this.labelUMH.Size = new System.Drawing.Size(54, 13);
            this.labelUMH.TabIndex = 53;
            this.labelUMH.Text = "labelUMH";
            this.labelUMH.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1075, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 54;
            this.label6.Text = "label6";
            this.label6.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1075, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 55;
            this.label7.Text = "label7";
            this.label7.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1075, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 56;
            this.label8.Text = "label8";
            this.label8.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1075, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 57;
            this.label5.Text = "label5";
            this.label5.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1075, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 58;
            this.label9.Text = "label9";
            this.label9.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1075, 105);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 59;
            this.label10.Text = "label10";
            this.label10.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1469, 966);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.labelUMH);
            this.Controls.Add(this.labeltss);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelif);
            this.Controls.Add(this.labelnp);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.selectedRows);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.zedGraphControl1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label Modlelable;
        private System.Windows.Forms.Label Datelabel;
        private System.Windows.Forms.Label VersionLable;
        private System.Windows.Forms.Label StartTimelabel;
        private System.Windows.Forms.Label Lengthlabel;
        private System.Windows.Forms.DataGridView dataGridView1;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.RadioButton KMH;
        private System.Windows.Forms.RadioButton MPH;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton PWShowBut;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton RPMShowBut;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton HRShowBut;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton SPShowBut;
        private System.Windows.Forms.RadioButton SPHideBut;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton ALTShowBut;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label Intervallabel;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label AvgALTlabel;
        private System.Windows.Forms.Label MaxALTlabel;
        private System.Windows.Forms.Label MinALTlabel;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label MinHRlabel;
        private System.Windows.Forms.Label AvgHRlabel;
        private System.Windows.Forms.Label MaxHRlabel;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label AvgPWlabel;
        private System.Windows.Forms.Label MaxPWlabel;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label AvgSPlabel;
        private System.Windows.Forms.Label MaxSPlabel;
        private System.Windows.Forms.Label Dicslabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton SpeedButton;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.Button selectedRows;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelnp;
        private System.Windows.Forms.Label labelif;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labeltss;
        private System.Windows.Forms.Label labelUMH;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}

