﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace CycleDataProgram
{
    public partial class Form1 : Form
    {      
        string Versiontype;
        string Montertype;

        string ModeTypeA;
        string ModeTypeB;
        string ModeTypeC;
        string ModeTypeD;
        string ModeTypeE;
        string ModeTypeF;
        string ModeTypeG;
        string ModeTypeH;
        string ModeTypeI;

        int SpeedType;
        int CadenceType;
        int AltType;
        int PowerType;
        int PowerLRBType;
        int PowerPIType;
        int HRCCType;
        int EuroUsType;
        int AirType;

        string DateYear;
        string DateMonth;
        string DateDay;
        string Fulldate;

        string StartHr;
        string StartMin;
        string StartSec;
        string FullStartTime;

        string LengthHr;
        string LengthMin;
        string LengthSec;
        double FullLengthInSecs;
        int HRRowCount;

        string Intervaltype;

        double SetMaxHR;
        int MaxHR;
        int MaxPW;
        int MaxALT;

        double MaxSP;
        double AvgSP;
        double MinSP;

        double AvgHR;             
        double AvgPW;
        double AvgALT;

        int MinHR;
        int MinPW;
        int MinALT;
        int Intervalval;

        int NP;
        double IF;

        double Dictence;
        int Versionval;
        GraphPane myPane;
        GraphPane myPane1;
       
        string filetext;

        /// <summary>
        /// load from1 and do functions,
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            zedGraphControl1.Visible = false;
        }


        /// <summary>
        /// On load click on the Open Button, open file dialoge.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
               
                // Launches the Open File Dialog
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = "hrm files (*.hrm)|*.hrm";
                openFileDialog1.FileName = "ASDBExampleCycleComputerData.hrm";
                openFileDialog1.ShowDialog();

                string strfilename = openFileDialog1.FileName;
                filetext = File.ReadAllText(strfilename);
                richTextBox1.Clear();
                dataGridView1.Columns.Clear();
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                richTextBox1.Text = filetext;
                Filetextread();
            }
            catch (Exception)
        {
            MessageBox.Show("Error: Could not read file from disk");
        }

        }
        /// <summary>
        /// Once the text has beed loaded reads the file, and sets the modes and calucaltes date
        /// </summary>
        public void Filetextread() {

            // Finds the Version and Sets the Data
            String FindVersion = "Version";
            int indexofVersion;
            indexofVersion = filetext.IndexOf(FindVersion);
            Versiontype = filetext.Substring(indexofVersion + 8, 3);
            Versionval = Int32.Parse(Versiontype);
            double version = double.Parse(Versiontype) / 100;
            VersionLable.Text = "Version: " + version.ToString("0.00");
            
            /////// Find and Sets The Monitor Modle
            String FindMonitor = "Monitor";
            int indexofMonitor;
            indexofMonitor = filetext.IndexOf(FindMonitor);
            Montertype = filetext.Substring(indexofMonitor + 8, 2);
            int Monterval = Int32.Parse(Montertype);
            if (Monterval == 1) { Modlelable.Text = "Model: " + "Polar Sport Tester / Vantage XL"; }
            if (Monterval == 2) { Modlelable.Text = "Model: " + "Polar Vantage NV (VNV)"; }
            if (Monterval == 3) { Modlelable.Text = "Model: " + "Polar Accurex Plus"; }
            if (Monterval == 4) { Modlelable.Text = "Model: " + "Polar XTrainer Plus"; }
            if (Monterval == 6) { Modlelable.Text = "Model: " + "Polar S520"; }
            if (Monterval == 7) { Modlelable.Text = "Model: " + "Polar Coach"; }
            if (Monterval == 8) { Modlelable.Text = "Model: " + "Polar S210"; }
            if (Monterval == 9) { Modlelable.Text = "Model: " + "Polar S410"; }
            if (Monterval == 10) { Modlelable.Text = "Model: " + "Polar S610 / S610i"; }
            if (Monterval == 12) { Modlelable.Text = "Model: " + "Polar S710 / S710i / S720i"; }
            if (Monterval == 13) { Modlelable.Text = "Model: " + "Polar S810 / S810i"; }
            if (Monterval == 15) { Modlelable.Text = "Model: " + "Polar E600"; }
            if (Monterval == 20) { Modlelable.Text = "Model: " + "Polar AXN500"; }
            if (Monterval == 21) { Modlelable.Text = "Model: " + "Polar AXN700"; }
            if (Monterval == 22) { Modlelable.Text = "Model: " + "Polar S625X / S725X"; }
            if (Monterval == 23) { Modlelable.Text = "Model: " + "Polar S725"; }
            if (Monterval == 33) { Modlelable.Text = "Model: " + "Polar CS400"; }
            if (Monterval == 34) { Modlelable.Text = "Model: " + "Polar CS600X"; }
            if (Monterval == 35) { Modlelable.Text = "Model: " + "Polar CS600"; }
            if (Monterval == 36) { Modlelable.Text = "Model: " + "Polar RS400"; }
            if (Monterval == 37) { Modlelable.Text = "Model: " + "Polar RS800"; }
            if (Monterval == 38) { Modlelable.Text = "Model: " + "Polar RS800X"; }

            // Finds the Version and Sets the Data
            String FindMaxHR = "MaxHR";
            int indexofFindMaxHR;
            indexofFindMaxHR = filetext.IndexOf(FindMaxHR);
            FindMaxHR = filetext.Substring(indexofFindMaxHR + 6, 3);
            SetMaxHR = Int32.Parse(FindMaxHR);
           

            ////// Find and Sets The Data Values            
            String FindDate = "Date";
            int indexofDate = filetext.IndexOf(FindDate);
            DateYear = filetext.Substring(indexofDate + 5, 4);
            DateMonth = filetext.Substring(indexofDate + 9, 2);
            DateDay = filetext.Substring(indexofDate + 11, 2);
            Fulldate = DateDay + " " + DateMonth + " " + DateYear;
            Datelabel.Text = "Date: " + Fulldate;
            
            ///// Finds and Sets the Start Time Values
            String FindStartTime = "StartTime=";
            int indexofStartTime = filetext.IndexOf(FindStartTime);
            StartHr = filetext.Substring(indexofStartTime + 10, 2);
            StartMin = filetext.Substring(indexofStartTime + 13, 2);
            StartSec = filetext.Substring(indexofStartTime + 16, 4);
            FullStartTime = StartHr + ":" + StartMin + ":" + StartSec;
            StartTimelabel.Text = "Start Time: " + FullStartTime;
            
            /////Find And sets Work out Length Values
            String FindLength = "Length=";
            int indexofLength = filetext.IndexOf(FindLength);
            LengthHr = filetext.Substring(indexofLength + 7, 2);
            LengthMin = filetext.Substring(indexofLength + 10, 2);
            LengthSec = filetext.Substring(indexofLength + 13, 5);
            int iLengthHr = Int32.Parse(LengthHr);
            int iLengthMin = Int32.Parse(LengthMin);
            double iLengthSec = double.Parse(LengthSec);
            FullLengthInSecs = ((iLengthHr * 3600) + (iLengthMin * 60) + (iLengthSec));
            Lengthlabel.Text = "Total Time: " + LengthHr + ":" + LengthMin + ":" + LengthSec;

            ///// Finds and Set Interval
            String FindInterval = "Interval";
            int indexofInterval;
            indexofInterval = filetext.IndexOf(FindInterval);
            Intervaltype = filetext.Substring(indexofInterval + 9, 2);
            Intervalval = Int32.Parse(Intervaltype);
            HRRowCount = (int)Math.Ceiling(FullLengthInSecs / Intervalval);
            Intervallabel.Text = "Interval: " + Intervaltype.ToString();

            DateTime date1 = new DateTime(2008, 8, 29, 0, 0, 0);

            ///////S-ModeSetUP HR DATA Reader & Setting DataGrid View
            string[] Data = Regex.Split(filetext, "HRData]");
            Data[1] = Data[1].Trim();
            string[] HRDatalines = Regex.Split(Data[1], "\r\n|\r|\n");
            dataGridView1.Columns.Add("Column", "Time");

            //Depending of What is Set in HR Data Hedder it will set MPH OR KM/H
            dataGridView1.Columns.Add("Column", "Hart Rate (BPM)");
            if (EuroUsType == 0)
            {
                dataGridView1.Columns.Add("Column", "Speed (KM/H)");
                MPH.Checked = false;
                KMH.Checked = true;
            }

            else if (EuroUsType == 1)
            {
                dataGridView1.Columns.Add("Column", "Speed (MPH");
                KMH.Checked = false;
                MPH.Checked = true;
            }
            //
            // This Secion Dose Diffrent Actions Acording to the verion of data recorder
            // if Version is 1.05
            if (Versionval <= 105)
            {
                String FindMode = "Mode";
                int indexofMode = filetext.IndexOf(FindMode);
                ModeTypeA = filetext.Substring(indexofMode + 5, 1);
                ModeTypeB = filetext.Substring(indexofMode + 6, 1);
                ModeTypeC = filetext.Substring(indexofMode + 7, 1);

                int ModeTypeA1 = Int32.Parse(ModeTypeA);
                if (ModeTypeA1 == 0)
                {
                    CadenceType = 1;
                    dataGridView1.Columns.Add("Column", "Cadence");
                }
                else if (ModeTypeA1 == 1)
                {
                    CadenceType = 0;
                    AltType = 1;
                    dataGridView1.Columns.Add("Column", "Altitude");
                }
                else if (ModeTypeA1 == 3)
                {
                    CadenceType = 0;
                    AltType = 0;
                    dataGridView1.Columns.Add("Column", "Setting Off");
                }
                HRCCType = Int32.Parse(ModeTypeB);
                EuroUsType = Int32.Parse(ModeTypeC);
                for (int i = 0; i < HRDatalines.Length; i++)
                {
                    date1 = date1.AddSeconds(Intervalval);
                    string[] HRDataCollum = Regex.Split(HRDatalines[i], "\t");
                    double speed123 = Int32.Parse(HRDataCollum[1]);
                    speed123 = speed123 / 10;
                    HRDataCollum[1] = speed123.ToString();
                    dataGridView1.Rows.Add(date1.ToString("HH:mm:ss"), HRDataCollum[0], HRDataCollum[1], HRDataCollum[2], HRDataCollum[3]);
                }
            }

            // IF Version is 1.06
            if (Versionval == 106)
            {
                String FindMode = "Mode";
                int indexofMode = filetext.IndexOf(FindMode);
                ModeTypeA = filetext.Substring(indexofMode + 5, 1);
                ModeTypeB = filetext.Substring(indexofMode + 6, 1);
                ModeTypeC = filetext.Substring(indexofMode + 7, 1);
                ModeTypeD = filetext.Substring(indexofMode + 8, 1);
                ModeTypeE = filetext.Substring(indexofMode + 9, 1);
                ModeTypeF = filetext.Substring(indexofMode + 10, 1);
                ModeTypeG = filetext.Substring(indexofMode + 11, 1);
                ModeTypeH = filetext.Substring(indexofMode + 12, 1);
                //Sets my Values to 1/0 from the Header informasion
                SpeedType = Int32.Parse(ModeTypeA);
                CadenceType = Int32.Parse(ModeTypeB);
                AltType = Int32.Parse(ModeTypeC);
                PowerType = Int32.Parse(ModeTypeD);
                PowerLRBType = Int32.Parse(ModeTypeE);
                PowerPIType = Int32.Parse(ModeTypeF);
                HRCCType = Int32.Parse(ModeTypeG);
                EuroUsType = Int32.Parse(ModeTypeH);
                // Sets my Colloums  
                dataGridView1.Columns.Add("Column", "Cadence (RPM)");
                dataGridView1.Columns.Add("Column", "Altitude(M/FT)");
                dataGridView1.Columns.Add("Column", "Power(Watts)");
                dataGridView1.Columns.Add("Column", "Power B&P");
                // adds my time to each collum
                
                for (int i = 0; i < HRDatalines.Length; i++)
                {
                    date1 = date1.AddSeconds(Intervalval);
                    string[] HRDataCollum = Regex.Split(HRDatalines[i], "\t");
                    double speed123 = Int32.Parse(HRDataCollum[1]);
                    speed123 = speed123 / 10;
                    HRDataCollum[1] = speed123.ToString();
                    dataGridView1.Rows.Add(date1.ToString("HH:mm:ss"), HRDataCollum[0], HRDataCollum[1], HRDataCollum[2], HRDataCollum[3], HRDataCollum[4], HRDataCollum[5]);
                }
                Workings();
                groupBox3.Visible = true;
                groupBox2.Visible = true;
                groupBox6.Visible = true;

                RPMShowBut.Checked = true;
                PWShowBut.Checked = false;
                ALTShowBut.Checked = false;
            }

            // IF Version is 1.07
            if (Versionval >= 107)
            {

                String FindMode = "Mode";
                int indexofMode = filetext.IndexOf(FindMode);

                ModeTypeA = filetext.Substring(indexofMode + 5, 1);
                ModeTypeB = filetext.Substring(indexofMode + 6, 1);
                ModeTypeC = filetext.Substring(indexofMode + 7, 1);
                ModeTypeD = filetext.Substring(indexofMode + 8, 1);
                ModeTypeE = filetext.Substring(indexofMode + 9, 1);
                ModeTypeF = filetext.Substring(indexofMode + 10, 1);
                ModeTypeG = filetext.Substring(indexofMode + 11, 1);
                ModeTypeH = filetext.Substring(indexofMode + 12, 1);
                ModeTypeI = filetext.Substring(indexofMode + 13, 1);
                //Sets my Values to 1/0 from the Header informasion
                SpeedType = Int32.Parse(ModeTypeA);
                CadenceType = Int32.Parse(ModeTypeB);
                AltType = Int32.Parse(ModeTypeC);
                PowerType = Int32.Parse(ModeTypeD);
                PowerLRBType = Int32.Parse(ModeTypeE);
                PowerPIType = Int32.Parse(ModeTypeF);
                HRCCType = Int32.Parse(ModeTypeG);
                EuroUsType = Int32.Parse(ModeTypeH);
                AirType = Int32.Parse(ModeTypeI);
                // Sets my Colloums
                dataGridView1.Columns.Add("Column", "Cadence (RPM)");
                dataGridView1.Columns.Add("Column", "Altitude(M/FT)");
                dataGridView1.Columns.Add("Column", "Power(Watts)");
                dataGridView1.Columns.Add("Column", "Power B&P");
                dataGridView1.Columns.Add("Column", "Air Pressure");
                // adds my time to each collum
                for (int i = 0; i < HRDatalines.Length; i++)
                {
                    date1 = date1.AddSeconds(Intervalval);
                    string[] HRDataCollum = Regex.Split(HRDatalines[i], "\t");
                    double speed123 = Int32.Parse(HRDataCollum[1]);
                    speed123 = speed123 / 10;
                    HRDataCollum[1] = speed123.ToString();
                    dataGridView1.Rows.Add(date1.ToString("HH:mm:ss"), HRDataCollum[0], HRDataCollum[1], HRDataCollum[2], HRDataCollum[3], HRDataCollum[4], HRDataCollum[5], HRDataCollum[6]);
                }
                Workings();
                groupBox3.Visible = true;
                groupBox2.Visible = true;
                groupBox6.Visible = true;

                RPMShowBut.Checked = true;
                PWShowBut.Checked = false;
                ALTShowBut.Checked = false;
            }

            groupBox.Visible = true;
            groupBox1.Visible = true;
            groupBox4.Visible = true;
            groupBox10.Visible = true;
            groupBox11.Visible = true;
            label2.Visible = true;
            textBox1.Visible = true;
            button1.Visible = true;
            selectedRows.Visible = true;

            HRShowBut.Checked = true;
            SPShowBut.Checked = true;


            /////////////////
            AvgHR = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                AvgHR += Convert.ToInt32(dataGridView1.Rows[i].Cells[1].Value);
            }
            AvgHR = AvgHR / dataGridView1.Rows.Count;

            /////////////////////
            MaxHR = dataGridView1.Rows.Cast<DataGridViewRow>()
                        .Max(r => Convert.ToInt32(r.Cells[1].Value));

            MinHR = dataGridView1.Rows.Cast<DataGridViewRow>()
                        .Min(r => Convert.ToInt32(r.Cells[1].Value));

            groupBox9.Visible = true;
            MaxHRlabel.Text = "Max: " + MaxHR.ToString("0");
            AvgHRlabel.Text = "Avg: " + AvgHR.ToString("0");
            MinHRlabel.Text = "Min: " + MinHR.ToString("0");

            AvrageSpeed();
            CreateGraph(zedGraphControl1);
            for (int i = 0; i < dataGridView1.ColumnCount; i++)

                dataGridView1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
        }


        /// <summary>
        /// this Secion Dose the workins for my Mins Max and Avrages, for Power, Altitued
        /// </summary>
        public void AvrageSpeed()
        {
            AvgSP = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                AvgSP += Convert.ToDouble(dataGridView1.Rows[i].Cells[2].Value);
            }
            AvgSP = AvgSP / dataGridView1.Rows.Count;

            MaxSP = dataGridView1.Rows.Cast<DataGridViewRow>()
                        .Max(r => Convert.ToDouble(r.Cells[2].Value));

            MinSP = dataGridView1.Rows.Cast<DataGridViewRow>()
                        .Min(r => Convert.ToDouble(r.Cells[2].Value));
            groupBox5.Visible = true;
            MaxSPlabel.Text = "Max: " + MaxSP.ToString("0.0");
            AvgSPlabel.Text = "Avg: " + AvgSP.ToString("0.0");
            //MinSPlabel.Text = "Min: " + MinSP.ToString("0.0");

            Dictence = ((AvgSP / 60) / 60) * FullLengthInSecs;
            if (MPH.Checked == true)
            {
                Dicslabel.Text = "Total Distance: " + Dictence.ToString("0.0") + "MPH";
            }
            if (KMH.Checked == true)
            {
                Dicslabel.Text = "Total Distance: " + Dictence.ToString("0.0") + "KM/H";
            }
        }





        /// <summary>
        /// this Secion Dose the workins for my Extra Colloms for Verion 1.05++
        /// Mins Max and Avrages, for Power, Altitued
        /// </summary>
        public void Workings()
        {
            /////////////////////
            AvgPW = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                AvgPW += Convert.ToInt32(dataGridView1.Rows[i].Cells[5].Value);
            }
            AvgPW = AvgPW / dataGridView1.Rows.Count;
            /////////////////////
            AvgALT = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                AvgALT += Convert.ToInt32(dataGridView1.Rows[i].Cells[4].Value);
            }
            AvgALT = AvgALT / dataGridView1.Rows.Count;

            MaxPW = dataGridView1.Rows.Cast<DataGridViewRow>()
                      .Max(r => Convert.ToInt32(r.Cells[5].Value));
            MaxALT = dataGridView1.Rows.Cast<DataGridViewRow>()
                        .Max(r => Convert.ToInt32(r.Cells[4].Value));
       
            MinPW = dataGridView1.Rows.Cast<DataGridViewRow>()
                        .Min(r => Convert.ToInt32(r.Cells[5].Value));
            MinALT = dataGridView1.Rows.Cast<DataGridViewRow>()
                        .Min(r => Convert.ToInt32(r.Cells[4].Value));

            groupBox7.Visible = true;           
            MaxPWlabel.Text = "Max: "+MaxPW.ToString("0.0");
            AvgPWlabel.Text = "Avg: "+AvgPW.ToString("0.0");
            //MinPWlabel.Text = "Min: " + MinPW.ToString("0.0");

            groupBox8.Visible = true;
            MaxALTlabel.Text = "Max: " + MaxALT.ToString("0");
            AvgALTlabel.Text = "Avg: " + AvgALT.ToString("0");
            MinALTlabel.Text = "Min: " + MinALT.ToString("0");

            List<int> initialPower = new List<int>();
            List<double> list = new List<double>();
            double total = 1;
            double average;

                try
                {
                for (int i = 1; i < dataGridView1.Rows.Count; i++)
                {
                    for (int n = 0; n < i + 30; n++)
                    {
                        initialPower.Add(Convert.ToInt32(dataGridView1.Rows[i + n].Cells[5].Value));
                    }
                    average = initialPower.Average();
                    total = Math.Pow(average, 4);
                    list.Add(total);
                    initialPower.Clear();
                }

            }
                catch (Exception)
                {
                average = list.Average();
                NP = Convert.ToInt32(Math.Pow(average, 1.0 / 4.00));
                labelnp.Text = NP.ToString();
                labelnp.Visible = true;
            }
        }




        /// <summary>
        /// Draws a Basic Graph, of Speed and Hart Rate also the X of Dictence, and Time
        /// </summary>
        private void CreateGraph(ZedGraphControl zgc)
        {
            zgc.GraphPane.CurveList.Clear();
            // get a reference to the GraphPane
            myPane = zgc.GraphPane;
            // Set the Titles
            myPane.Title = "Cycle Data Graph";
            myPane.YAxis.Title = "Vaule";

            double x, y1, y2;
            PointPairList list1 = new PointPairList();
            PointPairList list2 = new PointPairList();
            x = 0;

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (SpeedButton.Checked == true)
                {
                    x = x + (((Convert.ToDouble(dataGridView1.Rows[i].Cells[2].Value) / 60) / 60));
                    myPane.XAxis.Title = "Dictence Travled";
                }
                else { x = i; myPane.XAxis.Title = "Time In Seconds"; }
                y1 = Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value);
                y2 = Convert.ToDouble(dataGridView1.Rows[i].Cells[2].Value);
                list1.Add(x, y1);
                list2.Add(x, y2);
            }           
            if (HRShowBut.Checked == true)
            {
                LineItem curve;
                curve = myPane.AddCurve("Hart Rate", list1, Color.Blue, SymbolType.None);
                curve.Line.Width = 0.1F;
            }
            if (SPShowBut.Checked == true)
            {
                LineItem curve1;
                curve1 = myPane.AddCurve("Speed", list2, Color.Red, SymbolType.None);
                curve1.Line.Width = 0.1F;
            }
            //if the version is over 1.06 then run the extened Graph Method
            if (Versionval >= 106)
            {
                ExtraGraphdata();
            }

            zgc.AxisChange();
            zgc.Invalidate();
            zedGraphControl1.Visible = true;
        }

        /// <summary>
        /// Runs The extra Lines for Graph for Virsons of 1.05 ++
        /// </summary>
        private void ExtraGraphdata()
        {
            double x, y3, y4, y5;
            PointPairList list3 = new PointPairList();
            PointPairList list4 = new PointPairList();
            PointPairList list5 = new PointPairList();
            x = 0;
            for (int ii = 0; ii < dataGridView1.Rows.Count; ii++)
            {
                if (SpeedButton.Checked == true)
                {
                    x = x + (((Convert.ToDouble(dataGridView1.Rows[ii].Cells[2].Value) / 60) / 60));
                }
                else { x = ii; }
                y3 = Convert.ToDouble(dataGridView1.Rows[ii].Cells[3].Value);
                y4 = Convert.ToDouble(dataGridView1.Rows[ii].Cells[4].Value);
                y5 = Convert.ToDouble(dataGridView1.Rows[ii].Cells[5].Value);

                list3.Add(x, y3);
                list4.Add(x, y4);
                list5.Add(x, y5);
            }
            if (RPMShowBut.Checked == true)
            {
                LineItem curve2;
                curve2 = myPane.AddCurve("RPM", list3, Color.Green, SymbolType.None);
                curve2.Line.Width = 0.1F;
            }
            if (ALTShowBut.Checked == true)
            {
                LineItem curve3;
                curve3 = myPane.AddCurve("Altatued", list4, Color.SkyBlue, SymbolType.None);
                curve3.Line.Width = 0.1F;
            }
            if (PWShowBut.Checked == true)
            {
                LineItem curve4;
                curve4 = myPane.AddCurve("Power", list5, Color.DarkOrange, SymbolType.None);
                curve4.Line.Width = 0.1F;
            }
        }
        
     



        /// <summary>
        /// Converts MPH Units to KM/H when Button Is clicked
        /// </summary>
        private void KMH_MouseClick(object sender, MouseEventArgs e)
        {
            
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                double value = Convert.ToDouble(dataGridView1.Rows[i].Cells[2].Value);
                value = (value / 0.621371);
                string speed = value.ToString();
                dataGridView1[2, i].Value = value.ToString("0.0");
                dataGridView1.Columns[2].HeaderCell.Value = "Speed (KM/H)";              
            }
            AvrageSpeed();           
            CreateGraph(zedGraphControl1);
        }




        /// <summary>
        ///  Converts KM/H Units to MPH when Button Is clicked
        /// </summary>
        private void MPH_MouseClick(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                double value = Convert.ToDouble(dataGridView1.Rows[i].Cells[2].Value);              
                value = (value * 0.621371);
                string speed = value.ToString();
                dataGridView1[2, i].Value = value.ToString("0.0");
                dataGridView1.Columns[2].HeaderCell.Value = "Speed (MPH)";              
            }
            AvrageSpeed();           
            CreateGraph(zedGraphControl1);
        }

        /// <summary>
        /// Used When A button is changed to Redraw the graph.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HRShowBut_CheckedChanged(object sender, EventArgs e)
        {
            CreateGraph(zedGraphControl1);
        }
        /// <summary>
        /// When avrage data is selected user clicks the Slected data button, and will genrate avrages and display them in a new form window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectedRows_Click(object sender, EventArgs e)
        {
            Form2 hello = new Form2();
            Int32 selectedRowCount =
            dataGridView1.Rows.GetRowCount(DataGridViewElementStates.Selected);

            Double AvgSP1 = 0;
            for (int i = 0; i < selectedRowCount; ++i)
            {
                AvgSP1 += Convert.ToDouble(dataGridView1.SelectedRows[i].Cells[2].Value);
            }
            AvgSP1 = AvgSP1 / selectedRowCount;

            Double MaxSP1 = dataGridView1.SelectedRows.Cast<DataGridViewRow>()
                        .Max(r => Convert.ToDouble(r.Cells[2].Value));

            Double MinSP1 = dataGridView1.SelectedRows.Cast<DataGridViewRow>()
                        .Min(r => Convert.ToDouble(r.Cells[2].Value));
            hello.groupBox12.Visible = true;
            hello.label3.Text = "Max: " + MaxSP1.ToString("0.0");
            hello.label13.Text = "Avg: " + AvgSP1.ToString("0.0");
            hello.label2.Text = "Min: " + MinSP1.ToString("0.0");

            Double AvgHR1 = 0;
            for (int i = 0; i < selectedRowCount; ++i)
            {
                AvgHR1 += Convert.ToInt32(dataGridView1.SelectedRows[i].Cells[1].Value);
            }
            AvgHR1 = AvgHR1 / selectedRowCount;

            /////////////////////
            Double MaxHR1 = dataGridView1.SelectedRows.Cast<DataGridViewRow>()
                        .Max(r => Convert.ToInt32(r.Cells[1].Value));

            Double MinHR1 = dataGridView1.SelectedRows.Cast<DataGridViewRow>()
                        .Min(r => Convert.ToInt32(r.Cells[1].Value));

            hello.groupBox14.Visible = true;
            hello.label7.Text = "Max: " + MaxHR1.ToString("0");
            hello.label8.Text = "Avg: " + AvgHR1.ToString("0");
            hello.label6.Text = "Min: " + MinHR1.ToString("0");

            if (Versionval >= 106) {
                Double AvgPW1 = 0;
                for (int i = 0; i < selectedRowCount; ++i)
                {
                    AvgPW1 += Convert.ToInt32(dataGridView1.Rows[i].Cells[5].Value);
                }
                AvgPW1 = AvgPW1 / selectedRowCount;
                /////////////////////
                Double AvgALT1 = 0;
                for (int i = 0; i < selectedRowCount; ++i)
                {
                    AvgALT1 += Convert.ToInt32(dataGridView1.Rows[i].Cells[4].Value);
                }
               AvgALT1 = AvgALT1 / selectedRowCount;

                Double MaxPW1 = dataGridView1.SelectedRows.Cast<DataGridViewRow>()
                          .Max(r => Convert.ToInt32(r.Cells[5].Value));
                Double MaxALT1 = dataGridView1.SelectedRows.Cast<DataGridViewRow>()
                            .Max(r => Convert.ToInt32(r.Cells[4].Value));

                Double MinPW1 = dataGridView1.SelectedRows.Cast<DataGridViewRow>()
                            .Min(r => Convert.ToInt32(r.Cells[5].Value));
                Double MinALT1 = dataGridView1.SelectedRows.Cast<DataGridViewRow>()
                            .Min(r => Convert.ToInt32(r.Cells[4].Value));

                hello.groupBox13.Visible = true;
                hello.label5.Text = "Max: " + MaxPW1.ToString("0.0");
                hello.label4.Text = "Avg: " + AvgPW1.ToString("0.0");
                hello.label12.Text = "Min: " + MinPW1.ToString("0.0");

                hello.groupBox15.Visible = true;
                hello.label10.Text = "Max: " + MaxALT1.ToString("0");
                hello.label9.Text = "Avg: " + AvgALT1.ToString("0");
                hello.label11.Text = "Min: " + MinALT1.ToString("0");
            }
            CreateGraph1(hello.zedGraphControl11);
            hello.Show();
        }
       /// <summary>
       /// Is used to cerate the graph for the selected data
       /// </summary>
       /// <param name="zgc"></param>
        private void CreateGraph1(ZedGraphControl zgc)
        {
            zgc.GraphPane.CurveList.Clear();
            // get a reference to the GraphPane
            myPane1 = zgc.GraphPane;
            // Set the Titles
            myPane1.Title = "Cycle Data Graph";
            myPane1.YAxis.Title = "Vaule";

            double x, y1, y2, y3, y4 , y5;
            PointPairList list1 = new PointPairList();
            PointPairList list2 = new PointPairList();
            PointPairList list3 = new PointPairList();
            PointPairList list4 = new PointPairList();
            PointPairList list5 = new PointPairList();
          
            x = 0;

            for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
            {
               
                 x = i+Intervalval; myPane1.XAxis.Title = "Time In Seconds"; 
                y1 = Convert.ToDouble(dataGridView1.SelectedRows[i].Cells[1].Value);
                y2 = Convert.ToDouble(dataGridView1.SelectedRows[i].Cells[2].Value);
                list1.Add(x, y1);
                list2.Add(x, y2);
            }

               
            if (HRShowBut.Checked == true)
            {
                LineItem curve;
        curve = myPane1.AddCurve("Hart Rate", list1, Color.Blue, SymbolType.None);
                curve.Line.Width = 0.1F;
            }
            if (SPShowBut.Checked == true)
            {
                LineItem curve1;
    curve1 = myPane1.AddCurve("Speed", list2, Color.Red, SymbolType.None);
                curve1.Line.Width = 0.1F;
            }
            

            //if the version is over 1.06 then run the extened Graph Method
             if (Versionval >= 106)
              {

                for (int ii = 0; ii < dataGridView1.SelectedRows.Count; ii++)
                {
                    x = ii + Intervalval;
                y3 = Convert.ToDouble(dataGridView1.Rows[ii].Cells[3].Value);
                y4 = Convert.ToDouble(dataGridView1.Rows[ii].Cells[4].Value);
                y5 = Convert.ToDouble(dataGridView1.Rows[ii].Cells[5].Value);

                list3.Add(x, y3);
                list4.Add(x, y4);
                list5.Add(x, y5);

                   
                }
                if (RPMShowBut.Checked == true)
                {
                    LineItem curve2;
                    curve2 = myPane1.AddCurve("RPM", list3, Color.Green, SymbolType.None);
                    curve2.Line.Width = 0.1F;
                }
                if (ALTShowBut.Checked == true)
                {
                    LineItem curve3;
                    curve3 = myPane1.AddCurve("Altatued", list4, Color.SkyBlue, SymbolType.None);
                    curve3.Line.Width = 0.1F;
                }
                if (PWShowBut.Checked == true)
                {
                    LineItem curve4;
                    curve4 = myPane1.AddCurve("Power", list5, Color.DarkOrange, SymbolType.None);
                    curve4.Line.Width = 0.1F;
                }
            }
             
            zgc.AxisChange();
            zgc.Invalidate();
           // hello.zedGraphControl11.Visible = true;
        }
        /// <summary>
        /// saves text to a file, and form mate with Time as day, cerate a folder with DD/MM/Year as the folder name to orgoinzed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string ampm;
            try
            {
                if (Convert.ToInt32(StartHr) >= 12) {
                    ampm = "PM";
                }
                else { ampm = "AM"; }
                string content = richTextBox1.Text;
                string path = @"C:\cycledataprogram\" + DateDay +"-" +DateMonth + "-" + DateYear + "/";
                System.IO.Directory.CreateDirectory(path);
                File.WriteAllText(path + StartHr +  "."+ StartMin +" "+ampm + ".hrm", content);
                MessageBox.Show("Your day has been saved!");
            }
            catch (Exception)
            {
                MessageBox.Show("File not Saved");
            }
          
        }
        /// <summary>
        /// on click on load button show calnder view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            monthCalendar1.Visible=true;
        }

        /// <summary>
        /// when a date is slected from calderview load assosiated file text dialoge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            try
            {
                
                // Launches the Open File Dialog
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.InitialDirectory = @"C:\cycledataprogram\"+monthCalendar1.SelectionRange.Start.ToString("dd-MM-yyyy");
                openFileDialog1.Filter = "hrm files (*.hrm)|*.hrm";
                openFileDialog1.ShowDialog();

                string strfilename = openFileDialog1.FileName;
                filetext = File.ReadAllText(strfilename);
               
                richTextBox1.Clear();
                dataGridView1.Columns.Clear();
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                richTextBox1.Text = filetext;
                Filetextread();
            }
             catch (Exception)
             {
                 MessageBox.Show("File Not Opened");

             }
            monthCalendar1.Visible = false;

        }
        /// <summary>
        /// on button click get FTP value from user and calucalte TSS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
           double FTP = Convert.ToInt32(textBox1.Text);
           IF = NP / FTP;
           labelif.Text = IF.ToString("0.00");
            labelif.Visible = true;
            label3.Visible = true;
            double TSS;
            TSS = (FullLengthInSecs * NP * IF) / (FTP * 3600) * 100;
            labeltss.Text = "TSS: " + TSS.ToString("0.00");
            labeltss.Visible = true;
            label4.Visible = true;
            labeltss .Visible = true;

            labelUMH.Text = "User Set Max: " + SetMaxHR.ToString();

            label5.Text = "Zone 1 Recovery Zone: " + "< " + ((SetMaxHR / 100) * 68).ToString("0")  + "BPM     < " + ((FTP / 100) * 55).ToString("0") + "Power";
            label6.Text = "Zone 2 Endurance Zone: " + ((SetMaxHR / 100) * 69).ToString("0") + " - " + ((SetMaxHR / 100) * 83).ToString("0") + " BPM     " + ((FTP / 100) * 56).ToString("0") +" - " +((FTP / 100) * 75).ToString("0") + " Power";
            label7.Text = "Zone 3 Tempo Zone: " + ((SetMaxHR / 100) * 84).ToString("0") + " - " + ((SetMaxHR / 100) * 94).ToString("0") + " BPM     " + ((FTP / 100) * 76).ToString("0") + " - " + ((FTP / 100) * 90).ToString("0") + " Power";
            label8.Text = "Zone 4 Threshold Zone: " + ((SetMaxHR / 100) * 95).ToString("0") + "-" + ((SetMaxHR / 100) * 105).ToString("0") + " BPM     " + ((FTP / 100) * 95).ToString("0") + " - " + ((FTP / 100) * 105).ToString("0") + " Power";
            label9.Text = "Zone 5 VO2 max Zone: " + ((SetMaxHR / 100) * 106).ToString("0") + " - " + ((SetMaxHR / 100) * 121).ToString("0") + " BPM     " + ((FTP / 100) * 106).ToString("0") + " - " + ((FTP / 100) * 120).ToString("0") + " Power";
            label10.Text = "Zone 6 VO2 max Zone: " + ((SetMaxHR / 100) * 106).ToString("0") + " - " + ((SetMaxHR / 100) * 121).ToString("0") + " BPM     N/A";

            label5.Visible = true;
            label6.Visible = true;
            label7.Visible = true;
            label8.Visible = true;
            label9.Visible = true;
            label10.Visible = true;

        }


        /// <summary>
        /// when TSS box is clicked clear current text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
        }
    }
}

